$(document).ready(function () {

    var pstatus = $('#pstatus').val()
    if (pstatus == 'daftarBooks') {
        // alert(100)
        getFindBooks(0, 3, '')
    }

    if (pstatus == 'editBooks') {
        //    alert(100)
        bukaEditBooks()
    }
    if (pstatus == 'deleteBooks') {
        //    alert(100)
        bukaDeleteBooks()
    }

    $('#tombolCari').click(function () {
        var cari = $('#cariBooks').val()
        getFindBooks(0, 3, cari)

    })

    function getFindBooks(page, size, cari) {
        // var bioId = sessionStorage.getItem('bioId')
        $.ajax({
            url: 'http://localhost:8080/api/findbooks?page=' + page + '&size=' + size + '&cari=' + cari,
            type: 'GET',
            success: function (data) {
                var txt = ''
                for (i = 0; i < data.content.length; i++) {
                    txt += '<tr><td class = "input col-sm-1">' + data.content[i].code + '</td><td class = "input col-sm-3">' + data.content[i].title + '</td><td class = "input col-sm-3">' + data.content[i].author + '</td><td class = "input col-sm-3">' + data.content[i].publisher + '</td><td>' 
                    txt += '<div class="col-sm-2">'
                    txt += '<div class="btn-group"><button type="button" name="' + data.content[i].id + '"class="editBooksBtn btn-warning">U</button>'
                    txt += '<button type="button" name="' + data.content[i].id + '" class="deleteBooksBtn btn-danger">H</button>'
                    txt += '</div></div></td></tr>'

                }
                $('#getBooksList').html(txt)

                //pagination
                var tpage = data.totalPages
                ptxt = ''
                ptxt += '<div>'
                ptxt += '<button class="btn info"> << </button>'

                for (p = 1; p <= tpage; p++) {
                    ptxt += '<button class="pagingBtnBooks btn info" value="' + p + '"> ' + p + ' </button>'
                }
                ptxt += '<button class="btn info"> >> </button>'
                ptxt += '</div>'
                $('#tablePagination').html(ptxt)

                $('.pagingBtnBooks').click(function () {
                    var page = $(this).val() - 1
                    var cr = sessionStorage.getItem('cari')
                    if (cr == null) { cr = '' }
                    getFindBooks(page, 3, cr)
                })

                $('.deleteBooksBtn').click(function () {
                    // alert(1)
                    editId = $(this).attr('name')
                    //session storage
                    sessionStorage.setItem('editIds', editId)

                    bukaDeleteBooks()
                })

                $('.editBooksBtn').click(function () {
                    editId = $(this).attr('name')
                    //session storage
                    sessionStorage.setItem('editIds', editId)

                    bukaEditBooks()

                })
            }
        })
    }

    function bukaEditBooks() {
        var editIds = sessionStorage.getItem('editIds')

        $.ajax({
            url: 'http://localhost:8080/edit',
            type: 'GET',
            dataType: 'html',
            success: function (data) {
                $('#modalBooks').modal('toggle');
                $('#modalBooks').modal('show');
                $('.modal-body').html(data);
            }
        })
        $.ajax({
            url: 'http://localhost:8080/api/byid/' + editIds,
            type: 'GET',
            success: function (data) {
                // alert(data.name)
                $('#kodeEdit').val(data.code)
                $('#judulEdit').val(data.title)
                $('#penulisEdit').val(data.author)
                $('#penerbitEdit').val(data.publisher)

            }
        })
    }


    function bukaDeleteBooks() {
        var editIds = sessionStorage.getItem('editIds')

        $.ajax({
            url: 'http://localhost:8080/delete',
            type: 'GET',
            dataType: 'html',
            success: function (data) {
                $('#modalBooks').modal('toggle');
                $('#modalBooks').modal('show');
                $('.modal-body').html(data);
            }
        })

        $.ajax({
            url: 'http://localhost:8080/api/byid/' + editIds,
            type: 'GET',
            success: function (data) {
                // alert(data.name)
                let kode = data.code
                let judul = data.title
                let penulis = data.author
                let penerbit = data.publisher

                $('#kodeDelete').html(kode.substring(5, ))
                $('#judulDelete').html(judul.substring(5, ))
                $('#penulisDelete').html(penulis.substring(5, ))
                $('#penerbitDelete').html(penerbit.substring(5, ))
                console.log(data)

            }
        })
    }

    $('#hapusBooks').click(function () {
        var editIds = sessionStorage.getItem('editIds')
        $.ajax({
            url: 'http://localhost:8080/api/delete/' + editIds,
            type: 'DELETE',
            contentType: 'application/json',
            success: function (data) {
                console.log(data)
                alert("Data telah dihapus")
                $('#modalBooks').modal('hide')
                location.reload()
            }
        })
    })

    $('#tambahBooks').click(function () {

        $.ajax({
            url: 'http://localhost:8080/add',
            type: 'GET',
            dataType: 'html',
            success: function (data) {
                $('#modalBooks').modal('toggle')
                $('#modalBooks').modal('show')
                $('.modal-body').html(data)
            }
        })
    })

    $('#addBooks').click(function () {
        var kodeInput = document.getElementById('kodeBaru');
        var kodeError = document.getElementById('kodeAddError');
        var judulInput = document.getElementById('judulBaru');
        var judulError = document.getElementById('judulError');
        var penulisInput = document.getElementById('penulisBaru');
        var penulisError = document.getElementById('penulisError');
        var penerbitInput = document.getElementById('penerbitBaru');
        var penerbitError = document.getElementById('penerbitError');

        if (kodeInput.value.trim() === '') {
            kodeError.textContent = 'Kode tidak boleh kosong.';
            kodeError.style.display = 'block';
            event.preventDefault();
        } else if (judulInput.value.trim() === '') {
            judulError.textContent = 'Judul tidak boleh kosong.';
            judulError.style.display = 'block';
            event.preventDefault();
        } else if (penulisInput.value.trim() === '') {
            penulisError.textContent = 'Penulis tidak boleh kosong.';
            penulisError.style.display = 'block';
            event.preventDefault();
        } else if (penerbitInput.value.trim() === '') {
            penerbitError.textContent = 'Penerbit tidak boleh kosong.';
            penerbitError.style.display = 'block';
            event.preventDefault();
        } else {
            kodeError.textContent = '';
            kodeError.style.display = 'none';
            judulError.textContent = '';
            judulError.style.display = 'none';
            penulisError.textContent = '';
            penulisError.style.display = 'none';
            penerbitError.textContent = '';
            penerbitError.style.display = 'none';

            var obj = {}
            obj.code = $('#kodeBaru').val()
            obj.title = $('#judulBaru').val()
            obj.author = $('#penulisBaru').val()
            obj.publisher = $('#penerbitBaru').val()
            var dJson = JSON.stringify(obj);

            $.ajax({
                url: 'http://localhost:8080/api/add',
                type: 'POST',
                contentType: 'application/json',
                data: dJson,
                success: function (data) {
                    alert(data.result)
                    $('#modalBooks').modal('hide')
                    location.reload()
                }
            })
        }

    })


    $('#ubahBooks').click(function () {
        var editIds = sessionStorage.getItem('editIds');
        var kodeInput = document.getElementById('kodeEdit');
        var kodeError = document.getElementById('kodeEditError');
        var judulInput = document.getElementById('judulEdit');
        var judulError = document.getElementById('judulError');
        var penulisInput = document.getElementById('penulisEdit');
        var penulisError = document.getElementById('penulisError');
        var penerbitInput = document.getElementById('penerbitEdit');
        var penerbitError = document.getElementById('penerbitError');

        if (kodeInput.value.trim() === '') {
            kodeError.textContent = 'Kode tidak boleh kosong.';
            kodeError.style.display = 'block';
            event.preventDefault();
        } else if (judulInput.value.trim() === '') {
            judulError.textContent = 'Judul tidak boleh kosong.';
            judulError.style.display = 'block';
            event.preventDefault();
        } else if (penulisInput.value.trim() === '') {
            penulisError.textContent = 'Penulis tidak boleh kosong.';
            penulisError.style.display = 'block';
            event.preventDefault();
        } else if (penerbitInput.value.trim() === '') {
            penerbitError.textContent = 'Penerbit tidak boleh kosong.';
            penerbitError.style.display = 'block';
            event.preventDefault();
        } else {
            kodeError.textContent = '';
            kodeError.style.display = 'none';
            judulError.textContent = '';
            judulError.style.display = 'none';
            penulisError.textContent = '';
            penulisError.style.display = 'none';
            penerbitError.textContent = '';
            penerbitError.style.display = 'none';
            var obj = {}
            obj.code = $('#kodeEdit').val()
            obj.title = $('#judulEdit').val()
            obj.author = $('#penulisEdit').val()
            obj.publisher = $('#penerbitEdit').val()
            var dJson = JSON.stringify(obj);

            $.ajax({
                url: 'http://localhost:8080/api/edit/' + editIds,
                type: 'PUT',
                contentType: 'application/json',
                data: dJson,
                success: function (data) {
                    alert(data.result)
                    $('#modalBooks').modal('hide')
                    location.reload()
                }
            })
        }
    })
})