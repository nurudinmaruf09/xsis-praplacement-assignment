package com.example.crud.crud.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.crud.crud.models.PerpustakaanModel;
import com.example.crud.crud.repositories.PerpustakaanRepo;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class PerpustakaanRestControl {

    @Autowired
    PerpustakaanRepo crud;

     @GetMapping("/getall")
    public List<PerpustakaanModel> getBooks() {
        return crud.getBooks();
    }

     @GetMapping("/findbooks")
    public Page<PerpustakaanModel> getFindBooks(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "3") int size,
            @RequestParam(value = "cari", defaultValue = "") String cari) {

        Pageable pageable = PageRequest.of(page, size);
        return crud.getFindBooks(pageable, cari);

    }

     @GetMapping("/byid/{id}")
    public PerpustakaanModel getBooksById(@PathVariable long id) {
        return crud.getBooksById(id);
    }

    @PostMapping("/add")
    public Map<String, Object> addBooks(@Valid @RequestBody PerpustakaanModel p, BindingResult br) {
        Map<String, Object> result = new HashMap<>();
        // tangkap error dari @valid
        if (br.hasErrors()) {
            System.out.println(br.hasErrors());
            StringBuilder builder = new StringBuilder();
            List<FieldError> errors = br.getFieldErrors();
            for (FieldError fieldError : errors) {
                builder.append(fieldError.getDefaultMessage() + ", ");
            }
            result.put("result", builder.toString());
            return result;
        }

        // error lain
        try {
            crud.addBooks(p.getCode(), p.getTitle(), p.getAuthor(), p.getPublisher());
            result.put("result", "add success");
            return result;
        } catch (DataIntegrityViolationException e) {
            result.put("result", "Data tidak boleh kembar :" + e);
            return result;
        }
    }

    @PutMapping("/edit/{id}")
    public Map<String, Object> editBooks(@Valid @PathVariable long id, @RequestBody PerpustakaanModel p, BindingResult br) {
        Map<String, Object> result = new HashMap<>();

        if (br.hasErrors()) {
            System.out.println(br.hasErrors());
            StringBuilder builder = new StringBuilder();
            List<FieldError> errors = br.getFieldErrors();
            for (FieldError fieldError : errors) {
                builder.append(fieldError.getDefaultMessage() + ", ");
            }
            result.put("result", builder.toString());
            return result;
        }

        // error lain
        try {
            crud.editBooks(p.getCode(), p.getTitle(), p.getAuthor(), p.getPublisher(), id);
            result.put("result", "edit success");
            return result;
        } catch (DataIntegrityViolationException e) {
            result.put("result", "Data tidak boleh kembar :" + e);
            return result;
        }
    }

    @DeleteMapping("/delete/{id}")
    public void deleteBooks(@PathVariable long id) {

        crud.deleteBooks(id);
    }
}
