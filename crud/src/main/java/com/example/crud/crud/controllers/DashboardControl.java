package com.example.crud.crud.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DashboardControl {

    @GetMapping("/")
    public String home() {
        return "/dashboard/main";
    }

    @GetMapping("/add")
    public String spesial() {
        return "/dashboard/add";
    }

    @GetMapping("/edit")
    public String spesialedit() {
        return "/dashboard/edit";
    }

    @GetMapping("/delete")
    public String spesialDelete() {
        return "/dashboard/delete";
    }

}
