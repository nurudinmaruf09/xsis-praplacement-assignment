package com.example.crud.crud.repositories;

import java.util.List;

import jakarta.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.example.crud.crud.models.PerpustakaanModel;

@Transactional
public interface PerpustakaanRepo extends JpaRepository<PerpustakaanModel, Long> {
    @Query(value = "select * from crud_perpustakaan where is_delete=false order by id", nativeQuery = true)
    List<PerpustakaanModel> getBooks();

    @Query(value = "select * from crud_perpustakaan where id = :id and is_delete = false order by id", nativeQuery = true)
    PerpustakaanModel getBooksById(long id);

    // @Query(value = "select * from m_specialization where name ilike %:cari% and is_delete=false order by id", nativeQuery = true)
    // List<SpecializationModel> getCari( String cari);

    @Query(value = "select * from crud_perpustakaan where title ilike %:search% and is_delete = false order by id", nativeQuery = true)
    Page<PerpustakaanModel> getFindBooks(Pageable pageable, String search);

    @Modifying // jika query merubah data tambahkan anotation berikut
    @Query(value = "insert into crud_perpustakaan(code, title, author, publisher) values (:code, :title, :author, :publisher)", nativeQuery = true)
    void addBooks(String code, String title, String author, String publisher);

    @Modifying // jika query merubah data tambahkan anotation berikut
    @Query(value = "update crud_perpustakaan  set code=:code, title=:title, author=:author, publisher=:publisher where id=:id", nativeQuery = true)
    void editBooks(String code, String title, String author, String publisher, long id);

    @Modifying // jika query merubah data tambahkan anotation berikut
    @Query(value = "update crud_perpustakaan set is_delete=true, deleted_on=now() where id=:id", nativeQuery = true)
    void deleteBooks(long id);
}
