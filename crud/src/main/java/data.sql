insert into crud_perpustakaan(code, title, author, publisher)
values
('TTK001', 'The Great Gatsby', 'Scott Fitzgerald', 'Charles Scribner''s Sons'),
('TTK002', 'Don Quixote', 'Miguel de Cervantes', 'Francisco de Robles'),
('TTK003', 'To Kill a Mockingbird', 'Harper Lee', 'J. B. Lippincott & Co.');