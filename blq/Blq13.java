import java.util.Scanner;

public class Blq13 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan jarum panjang jam analog: ");
        int h = input.nextInt();
        System.out.print("Masukkan jarum pendek jam analog: ");
        int m = input.nextInt();

        input.close();
        // validate the input
        if (h < 0 || m < 0 || h > 12 || m > 60)
            System.out.println("Wrong input");

        if (h == 12) h = 0;
        if (m == 60) {
            m = 0;
            h += 1;
            if (h > 12) h = h - 12;
        }

        // Calculate the angles moved by hour and minute hands
        // with reference to 12:00
        int sudutJam = (int) (0.5 * (h * 60 + m));
        int sudutMenit = (int) (6 * m);

        // Find the difference between two angles
        int sudut = Math.abs(sudutJam - sudutMenit);

        // smaller angle of two possible angles
        sudut = Math.min(360 - sudut, sudut);

        System.out.println("\nBesar sudut yang terbentuk adalah: " + sudut);
    }
}
