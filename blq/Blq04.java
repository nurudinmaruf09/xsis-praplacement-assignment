import java.util.Scanner;

public class Blq04 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int bil, n;

        System.out.print("N : ");
        n = input.nextInt();

        input.close();
        for (int i = 1; i <= n; i++) {

            if (i == 0 | i == 1){
                continue;
            }

            bil = 0;
            for (int j = 1; j <= i; j++) {
                if (i % j == 0) {
                    bil = bil + 1;
                }
            }
            if (bil == 2) {
                System.out.print(i + " ");
            }
        }
    }
}
