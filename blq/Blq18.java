public class Blq18 {
    public static void main(String[] args) {

        int[] time = { 9, 13, 15, 17 };
        int[] calories = { 30, 20, 50, 80 };
        int start = 18;
        int diff;
        double workoutTime = 0;

        for (int i = 0; i < time.length; i++) {
            diff = start - time[i];
            workoutTime += 0.1 * calories[i] * diff;
        }

        int drinkWater = 500;
        if (workoutTime > 30) {
            drinkWater += (int) (100 * Math.floor(workoutTime / 30));
        }

        System.out.println("Air yang akan diminum sepanjang olahraga: " + drinkWater + " cc");
    }

}
