import java.util.Scanner;

public class Blq22 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan deret angka integer: ");
        String deret = input.nextLine();

        String[] arrDeret = deret.split(" ");

        int[] arrDeretInt = new int[arrDeret.length];

        input.close();

        for (int i = 0; i < arrDeret.length; i++) {
            try {
                arrDeretInt[i] = Integer.parseInt(arrDeret[i]);
            } catch (NumberFormatException e) {
                System.out.println("Error: Input dengan non-integer terdeteksi " + e);
                break;
            }
        }

        for (int i = 0; i < arrDeretInt.length; i++){
            for (int j = 0; j < arrDeretInt.length; j++){
                arrDeretInt[j] -= 1;
            }
            if(arrDeretInt[i] == 0) break;
        }

        int targetValue = 0;

        System.out.print("Lilin yang meleleh paling cepat adalah lilin ke-");
        for (int i = 0; i < arrDeretInt.length; i++) {
            if (arrDeretInt[i] == targetValue) {
                System.out.print((i+1) + ", ");
            }
        }

    }
}
