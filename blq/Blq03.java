import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Blq03 {
    public static void main(String[] args) {

        String tglA = "27/01/2020 10:03:34";
        String tglB = "28/01/2020 18:03:35";
        long selisih = 0, hari = 0, jam = 0, jamUnit = 0, menit = 0, detik = 0;
        long bayar = 0;
        // Buat format
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        // Proses Membuat selisih antar jam
        try {
            Date dTglA = format.parse(tglA);
            Date dTglB = format.parse(tglB);
            selisih = dTglB.getTime() - dTglA.getTime();

            hari = TimeUnit.MILLISECONDS.toDays(selisih);
            jam = TimeUnit.MILLISECONDS.toHours(selisih) -
            TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(selisih));

            jamUnit = TimeUnit.MILLISECONDS.toHours(selisih);

            menit = TimeUnit.MILLISECONDS.toMinutes(selisih)
                    - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(selisih));
            detik = TimeUnit.MILLISECONDS.toSeconds(selisih)
                    - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(selisih));

            System.out.println("Selisih Hari (dalam satuan jam) : " + jamUnit);
            System.out.println("Selisih Hari                    : " + hari);
            System.out.println("Selisih Jam                     : " + jam);
            System.out.println("Selisih Menit                   : " + menit);
            System.out.println("Selisih Detik                   : " + detik);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        // Proses untuk menghitung tarif parkir
        if (hari == 0 && jam <= 1) {
            bayar += 1000 + ((jam - 8) * 1000);
        }
        else if (hari == 0 && jam >= 8) {
            bayar += 8000;
        }  
        else if (hari >= 1 && (jam <= 8 && detik == 0)) {
            bayar += 15000 + (jam * 1000);
        }

        System.out.println("Biaya Parkir : " + bayar);
    }
}
