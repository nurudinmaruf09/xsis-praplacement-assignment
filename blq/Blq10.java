import java.util.Scanner;

public class Blq10 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan string: ");
        String str = input.nextLine();

        String[] arrStr = str.split(" ");

        input.close();

        for(int i = 0; i < arrStr.length; i++){
            if(arrStr[i].length() > 2){
                arrStr[i] = arrStr[i].replace(arrStr[i].substring(1, arrStr[i].length() - 1), "***");
            }
            System.out.print(arrStr[i] + " ");
        }
    }
}
