import java.util.Random;
import java.util.Scanner;

public class Blq20 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int distance = 2, winStep = 2, loseStep = 1;

        while (true) {
            System.out.println("Make a move! (rock/paper/scissors)");
            String playerMove = scanner.nextLine();

            // Getting input from the computer
            Random random = new Random();
            int randomNumber = random.nextInt(3);

            String computerMove;
            if (randomNumber == 0) {
                computerMove = "rock";
            } else if (randomNumber == 1) {
                computerMove = "paper";
            } else {
                computerMove = "scissors";
            }
            System.out.println("Computer chose " + computerMove + "!\n");

            // Print results
            if (playerMove.equals(computerMove)) {
                System.out.println("It's a draw!\n");
            } else if (playerWins(playerMove, computerMove)) {
                distance += loseStep - winStep;
                if (distance == 0) {
                    System.out.println("The distance between players is: " + distance);
                    System.out.println("Player wins!\n");
                    break;
                } else {
                    System.out.println("The distance between players is: " + distance + "\n");
                }
            } else {
                distance += loseStep - winStep;
                if (distance == 0) {
                    System.out.println("The distance between players is: " + distance);
                    System.out.println("Computer wins!\n");
                    break;
                } else {
                    System.out.println("The distance between players is: " + distance + "\n");
                }
            }
        }

        scanner.close();
    }

    static boolean playerWins(String playerMove, String computerMove) {
        if (playerMove.equals("rock")) {
            return computerMove.equals("scissors");
        } else if (playerMove.equals("paper")) {
            return computerMove.equals("rock");
        } else {
            return computerMove.equals("paper");
        }
    }

}
