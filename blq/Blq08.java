import java.util.Arrays;
import java.util.Scanner;

public class Blq08 {
    public static void main(String[] args) {

        int minSum = 0, maxSum = 0;

        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan deret angka integer: ");
        String deret = input.nextLine();
        System.out.print("Masukkan batas komponen deret yang akan dijumlahkan: ");
        int batas = input.nextInt();

        String[] arrDeret = deret.split(" ");

        int[] arrDeretInt = new int[arrDeret.length];

        input.close();

        // Check if the sequence has string while
        // converting user input string into integer
        for (int i = 0; i < arrDeret.length; i++) {
            try {
                arrDeretInt[i] = Integer.parseInt(arrDeret[i]);
            } catch (NumberFormatException e) {
                System.out.println("Error: Input dengan non-integer terdeteksi " + e);
                break;
            }
        }

        // sort array
        Arrays.sort(arrDeretInt);
        System.out.println(Arrays.toString(arrDeretInt));

        // finding minimal sum value
        for (int i = 0; i < batas; i++){
            minSum += arrDeretInt[i];
        }

        System.out.println("Nilai penjumlahan minimal dari deret yang dimasukkan adalah: " + minSum);

        // finding maximal sum value
        for (int i = batas-1; i < arrDeretInt.length; i++){
            maxSum += arrDeretInt[i];
        }

        System.out.println("Nilai penjumlahan maksimal dari deret yang dimasukkan adalah: " + maxSum);
    }
}
