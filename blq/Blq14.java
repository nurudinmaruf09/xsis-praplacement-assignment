import java.util.Arrays;
import java.util.Scanner;

public class Blq14 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan deret angka integer: ");
        String deret = input.nextLine();
        System.out.print("N: ");
        int pecah = input.nextInt();

        String[] arrDeret = deret.split(" ");

        int[] arrDeretInt = new int[arrDeret.length];

        input.close();

        // Check if the sequence has string while
        // converting user input string into integer
        for (int i = 0; i < arrDeret.length; i++) {
            try {
                arrDeretInt[i] = Integer.parseInt(arrDeret[i]);
            } catch (NumberFormatException e) {
                System.out.println("Error: Input dengan non-integer terdeteksi " + e);
                break;
            }
        }

        System.out.println("Deret awal: " + Arrays.toString(arrDeretInt));

        int n = arrDeretInt.length;
        int rotasiIndex = (n - pecah) % n;
        int[] hasilRotasi = new int[n];

        for (int i = 0; i < n; i++) {
            hasilRotasi[(i + rotasiIndex) % n] = arrDeretInt[i];
        }

        System.arraycopy(hasilRotasi, 0, deret, 0, n);

        System.out.println("Hasil rotasi: " + Arrays.toString(arrDeretInt));
    }

}
