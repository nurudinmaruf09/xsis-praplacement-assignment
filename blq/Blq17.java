public class Blq17 {
    public static void main(String[] args) {
        
        String aJalan[] = "N N T N N N T T T T T N T T T N T N N N N N T T ".split(" ");

        int mdpl = 0;
        int Perjalanan = 0;
        int gunung = 0;
        int lembah = 0;

        for (int i = 0; i < aJalan.length; i++) {
            if (aJalan[i].equalsIgnoreCase("N")) {// Track Ketemu N
                mdpl++;
            } else if (aJalan[i].equalsIgnoreCase("T")) {//Track Ketemu T
                mdpl--;
            }
            // Catat Perjalanan
            if (mdpl > 0) {
                Perjalanan = 1;
            } else if (mdpl < 0) {
                Perjalanan = -1;
            }
            // Catat Jumlah gunung dan Lembah
            if (Perjalanan == 1 && mdpl == 0) {
                gunung++;
                Perjalanan = 0;
            } else if (Perjalanan == -1 && mdpl == 0) {
                lembah++;
                Perjalanan = 0;
            }
        }
        System.out.println("Gunung : " + gunung);
        System.out.println("Lembah : " + lembah);
    }
}
