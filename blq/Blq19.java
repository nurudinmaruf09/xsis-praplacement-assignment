import java.util.Scanner;

public class Blq19 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan String yang akan dicek: ");
        String str = input.nextLine();
        input.close();

        // convert all user input string to lower case
        str = str.toLowerCase();
        
        boolean isPangram = true;
        for(char ch = 'a'; ch <= 'z'; ch++) {
            if(!str.contains(String.valueOf(ch))){
                isPangram = false;
                break;
            } else isPangram = true;
        }

        if (isPangram){
            System.out.println("\"" + str + "\" adalah pangram");
        } else {
            System.out.println("\"" + str + "\" bukanlah pangram");
        }


    }
}
