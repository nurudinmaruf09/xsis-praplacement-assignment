import java.util.Scanner;

public class Blq06 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan string yang akan dicek : ");
        String string = input.nextLine();
        input.close();

        String reverse = "";

        int strLength = string.length();

        for (int i = (strLength - 1); i >= 0; --i) {
            reverse = reverse + string.charAt(i);
        }

        if (string.toLowerCase().equals(reverse.toLowerCase())) {
            System.out.println("\"" + string + "\" adalah palindrome");
        } else {
            System.out.println("\"" + string + "\" bukanlah palindrome");
        }
    }
}
