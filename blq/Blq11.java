import java.util.Scanner;

public class Blq11 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Input: ");

        String str = input.nextLine();
        String[] arrStr = str.split("");
        String bintang = "";

        for(int i = 0; i < arrStr.length / 2; ++i) {
            bintang +=  "*";
        }

        System.out.println("Output: ");

        for(int i = arrStr.length - 1; i >= 0; --i) {
            System.out.println(bintang + arrStr[i] + bintang);
        }

        input.close();
    }

}