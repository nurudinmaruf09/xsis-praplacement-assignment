import java.util.Arrays;
import java.util.Scanner;

public class Blq07 {
    public static void main(String[] args) {

        int median = 0, medianGenap1 = 0, medianGenap2 = 0, total = 0, maxcount = 0, freqElement = 0;
        float rataRata = 0, medianGenap = 0;

        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan deret angka integer: ");
        String deret = input.nextLine();

        String[] arrDeret = deret.split(" ");

        int[] arrDeretInt = new int[arrDeret.length];

        input.close();

        // Check if the sequence has string while
        // converting user input string into integer
        for (int i = 0; i < arrDeret.length; i++) {
            try {
                arrDeretInt[i] = Integer.parseInt(arrDeret[i]);
            } catch (NumberFormatException e) {
                System.out.println("Error: Input dengan non-integer terdeteksi " + e);
                break;
            }
        }

        // sort array
        Arrays.sort(arrDeretInt);
        System.out.println(Arrays.toString(arrDeretInt));

        // find the average value
        for (int i = 0; i < arrDeretInt.length; i++) {
            total += arrDeretInt[i];
        }

        rataRata = (float) total / arrDeretInt.length;
        System.out.println("\nNilai Rata-rata dari Array adalah " + rataRata);

        // find the median value
        if (arrDeretInt.length % 2 == 0) {
            medianGenap1 = (arrDeretInt.length / 2) - 1;
            medianGenap2 = (arrDeretInt.length / 2);

            medianGenap = (float) (arrDeretInt[medianGenap1] + arrDeretInt[medianGenap2]) / 2;
            System.out.println("Nilai median dari Array adalah " + medianGenap);
        } else if (arrDeretInt.length % 2 == 1) {
            median = arrDeretInt.length / 2;
            System.out.println("Nilai median dari Array adalah " + arrDeretInt[median]);
        }

        // find the most frequent array's element
        for (int i = 0; i < arrDeretInt.length; i++) {
            int count = 0;
            for (int j = 0; j < arrDeretInt.length; j++) {
                if (arrDeretInt[i] == arrDeretInt[j]) {
                    count++;
                }
            }

            if (count > maxcount) {
                maxcount = count;
                freqElement = arrDeretInt[i];
            }
        }
        System.out.println("Nilai modus dari Array adalah " + freqElement);

    }
}
