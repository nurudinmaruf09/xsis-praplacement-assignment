import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Blq15 {

    // ParseException used if the user input doesn't match with expected format
    public static void main(String[] args) throws ParseException {

        Scanner input = new Scanner(System.in);
        System.out.print("Enter time to be converted (hh:mm:ss aa): ");
        String time = input.nextLine();
        input.close();

        // 12 hour format
        DateFormat format12Hour = new SimpleDateFormat("hh:mm:ss aa");

        // 24 hour format
        DateFormat formatMilitary = new SimpleDateFormat("HH:mm:ss");

        // parse input to date format
        Date twelveHour = format12Hour.parse(time);
        // change the date format and storing it in String
        String militaryHour = formatMilitary.format(twelveHour);

        System.out.println("The converted time format is " + militaryHour);

    }
}
