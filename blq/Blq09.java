import java.util.Scanner;

public class Blq09 {
    public static void main(String[] args) {
        
        Scanner input = new Scanner(System.in);
        System.out.print("N: ");
        int n = input.nextInt();
        input.close();

        for(int i = 1; i <= n; i++){
            System.out.print(n * i + " ");
        }
    }
}
