import java.util.Scanner;

public class Blq12 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan deret angka integer yang akan disorting: ");
        String deret = input.nextLine();

        String[] arrDeret = deret.split(" ");

        int[] arrDeretInt = new int[arrDeret.length];

        input.close();
        
        // Check if the sequence has string while
        // converting user input string into integer
        for (int i = 0; i < arrDeret.length; i++) {
            try {
                arrDeretInt[i] = Integer.parseInt(arrDeret[i]);
            } catch (NumberFormatException e){
                System.out.println("Error: Input dengan non-integer terdeteksi " + e);
                break;
            }
        }

        // Sorting using bubble sort algorithm
        for (int i = 0; i < arrDeretInt.length; i++) {
            for (int j = i + 1; j < arrDeretInt.length; j++) {
                int temp = 0;
                if (arrDeretInt[i] > arrDeretInt[j]) {
                    temp = arrDeretInt[i];
                    arrDeretInt[i] = arrDeretInt[j];
                    arrDeretInt[j] = temp;
                }
            }
            System.out.print(arrDeretInt[i] + " ");
        }
    }
}
